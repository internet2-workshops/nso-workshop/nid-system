# NSO Workshop - Internet2 Community Exchange 2023

## 1. Intro to Workshop Environment

### 1.1 Connect to VM

```sh
ssh {username}@nso-workshop-{id}.macc.ns.internet2.edu
```

### 1.2 Connect with VS Code

```sh
# modify .ssh/config
```

### 1.3 Explore and start NSO environment

```sh
# Already cloned
# git clone https://gitlab.com/internet2-workshops/nso-workshop/nid-system.git
cd nid-system
git fetch
ls
make build
docker image ls
make testenv-start
docker ps -a
```

### 1.4 Explore NSO

```sh
make testenv-cli
show packages package oper-status
show configuration devices
show configuration device device xr0
show configuration services
```

## 2. Creating an NSO Service

### 2.1 Create NSO skeleton package

```sh
cd packages/
ncs-make-package --service-skeleton python-and-template l3vpn
exit
```

### *Escape hatch*

```sh
git checkout -f 01-package-created
```

### 2.2 Explore package structure

```sh
ls packages/l3vpn
# use VS Code
make testenv-rebuild
make testenv-cli
show packages package oper-status

configure
edit l3vpn EXAMPLE123
# add service config
commit dry-run
exit
exit
# Discard changes
```

### 2.3 Modify YANG

Copy contents from [lab_material/l3vpn.yang](lab_material/l3vpn.yang)

```sh
# edit packages/l3vpn/src/l3vpn.yang
make testenv-rebuild
make testenv-cli

configure
edit l3vpn EXAMPLE123
# add service config
commit dry-run
commit | details
```

### *Escape hatch*

```sh
git checkout -f 02-yang-modified
```

### 2.4 Modify template

Copy contents from [lab_material/l3vpn-template.xml](lab_material/l3vpn-template.xml)

```sh
# edit packages/l3vpn/templates/l3vpn-template.xml
make testenv-rebuild
make testenv-cli
request l3vpn EXAMPLE123 re-deploy dry-run
request l3vpn EXAMPLE123 re-deploy | detail
request l3vpn EXAMPLE123 get-modifications
```

### *Escape hatch*

```sh
git checkout -f 03-template-modified
```

## 3. JunOS Quick Reference

- `tab` / `?` completion
- Exec mode
  - `show config {X}`
  - `request {X}`
- Config mode
  - `edit {X}` / `up|top`
  - `set` / `delete`
  - `show`, `show {X}`
  - `show | compare`
  - `show | display set`
  - `run [CMD]`
  - `request {X}...`
